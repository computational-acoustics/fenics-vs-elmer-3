import pathlib
import fenics
import typing
import numpy as np
import meshio
from acoupy_meshutil import readers
from acoupy_helmholtz import solver
import h5py


_repo_home = pathlib.Path(__file__).parent.parent
_mesh_dir = _repo_home.joinpath('mesh')
_elmer_dir = _repo_home.joinpath('elmer')
_error_dir = _repo_home.joinpath('error_study')


def elmer_read(file: pathlib.Path) -> typing.Tuple[np.ndarray, np.ndarray]:
    elmer_data = meshio.read(file)
    mesh_nodes = elmer_data.points
    u_at_nodes = elmer_data.point_data['pressure wave 1'] + 1j * elmer_data.point_data['pressure wave 2']
    assert np.prod(u_at_nodes.shape) == u_at_nodes.shape[0]
    return mesh_nodes, u_at_nodes.flatten()


def fenics_simulate(
        file: pathlib.Path,
        speed_of_sound: float = 343.0,
        density: float = 1.205,
        outer_radius: float = 0.1,
        velocity_mag: float = 0.75,
        velocity_ang: float = 0.7853981633974483,
        frequency: float = 1000.0
) -> typing.Tuple[np.ndarray, np.ndarray]:

    mesh_reader = readers.Reader(
        file_format=readers.Reader.Format.MED,
        dimension=readers.Reader.Dimension.D_3D,
        coordinate_scaling=0.001
    )

    s, e = mesh_reader.read(path=file)
    if not s:
        raise e

    tags = mesh_reader.get_cell_tags()
    assert tags == {0: ['Outlet'], 1: ['Inlet']}

    mesh, body_markers, sub_domains_markers = mesh_reader.get_mesh()
    dx_volumes = fenics.Measure('dx', domain=mesh, subdomain_data=body_markers)
    ds_surfaces = fenics.Measure('ds', domain=mesh, subdomain_data=sub_domains_markers[0])

    medium = solver.MediumSettings(
        speed_of_sound=speed_of_sound,
        density=density
    )

    wave_number = 2.0 * np.pi * frequency / medium.speed_of_sound
    source_velocity = velocity_mag * np.exp(1j * velocity_ang)
    outlet_impedance = (
            medium.density * medium.speed_of_sound * (1.0j * wave_number * outer_radius) /
            (1.0 + 1.0j * wave_number * outer_radius)
    )

    solution = solver.solve(
        mesh=mesh,
        simulation_settings=solver.SimulationSettings(frequency=frequency, medium_settings=medium),
        main_domain_settings=solver.MainDomainSettings(measure=dx_volumes),
        element_settings=solver.ElementSettings(family='P', degree=2),
        solver_settings=solver.SolverSettings(
            linear_solver='gmres',
            preconditioner='ilu',
            absolute_tolerance=1e-10,
            maximum_iterations=10000,
            monitor_convergence=True

        ),
        uniform_neumann_bcs=solver.UniformNeumannBCS(
            [
                solver.UniformNeumannBC(velocity=source_velocity, measure=ds_surfaces(1))
            ]
        ),
        uniform_robin_bcs=solver.UniformRobinBCS(
            [
                solver.UniformRobinBC(impedance=outlet_impedance, velocity=0.0, measure=ds_surfaces(0))
            ]
        ),
    )

    u_re, u_im = solution.split(deepcopy=True)

    # Inspired by this, but different: https://fenicsproject.org/pub/tutorial/html/._ftut1019.html
    # The solutions can be called to interpolate over values: just leverage that.
    mesh_nodes = mesh.coordinates()
    u_at_nodes = np.zeros(mesh_nodes.shape[0], dtype=complex)
    for r in range(mesh_nodes.shape[0]):
        u_at_nodes[r] = u_re(*mesh_nodes[r, :]) + 1j * u_im(*mesh_nodes[r, :])

    return mesh_nodes, u_at_nodes


def compute_exact(
        mesh_nodes: np.ndarray,
        speed_of_sound: float = 343.0,
        density: float = 1.205,
        inner_radius: float = 0.005,
        velocity_mag: float = 0.75,
        velocity_ang: float = 0.7853981633974483,
        frequency: float = 1000.0
) -> np.ndarray:

    radii = np.sqrt(np.sum(mesh_nodes ** 2, axis=1))

    wave_number = 2.0 * np.pi * frequency / speed_of_sound
    source_velocity = velocity_mag * np.exp(1j * velocity_ang)

    source_impedance = (
            density * speed_of_sound * (1.0j * wave_number * inner_radius) /
            (1.0 + 1.0j * wave_number * inner_radius)
    )

    u_at_nodes = (
            source_impedance * inner_radius * source_velocity *
            np.exp(-1j * wave_number * (radii - inner_radius)) / radii
    )

    return u_at_nodes


def make_study(elmer_file: pathlib.Path, mesh_file: pathlib.Path, flag: str) -> None:
    elmer_mesh, elmer_sol = elmer_read(file=elmer_file)
    elmer_exact = compute_exact(elmer_mesh)
    elmer_radii = np.sqrt(np.sum(elmer_mesh ** 2, axis=1))

    fenics_mesh, fenics_sol = fenics_simulate(file=mesh_file)
    fenics_exact = compute_exact(fenics_mesh)
    fenics_radii = np.sqrt(np.sum(fenics_mesh ** 2, axis=1))

    exact_radii = np.linspace(
        max(np.min(elmer_radii), np.min(fenics_radii)),
        min(np.max(elmer_radii), np.max(fenics_radii)),
        4096
    )
    exact_ref = compute_exact(mesh_nodes=exact_radii.reshape((-1, 1)))

    with h5py.File(_error_dir.joinpath('{:s}_study.hdf5'.format(flag)), 'w') as f:
        elmer_group = f.create_group(name='elmer')
        elmer_group.create_dataset(name='mesh', data=elmer_mesh)
        elmer_group.create_dataset(name='solution', data=elmer_sol)
        elmer_group.create_dataset(name='exact', data=elmer_exact)

        fenics_group = f.create_group(name='fenics')
        fenics_group.create_dataset(name='mesh', data=fenics_mesh)
        fenics_group.create_dataset(name='solution', data=fenics_sol)
        fenics_group.create_dataset(name='exact', data=fenics_exact)

        exact_group = f.create_group(name='exact')
        exact_group.create_dataset(name='radii', data=exact_radii)
        exact_group.create_dataset(name='solution', data=exact_ref)


if __name__ == '__main__':
    make_study(
        elmer_file=_elmer_dir.joinpath('case_t0001.vtu'),
        mesh_file=_mesh_dir.joinpath('mesh-5mm.med'),
        flag='5mm'
    )
