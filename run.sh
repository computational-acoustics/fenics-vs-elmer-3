#!/bin/bash

start_time=$(date -u +%s)
cd elmer/ && ElmerSolver && cd ../
if [ $? -ne 0 ]
then
    exit 1
fi
stop_time=$(date -u +%s)
elapsed=$(($stop_time - $start_time))
echo "Elmer took $elapsed seconds to complete."

start_time=$(date -u +%s)
python/venv/bin/python python/simulate_and_compare.py
if [ $? -ne 0 ]
then
    exit 1
fi
stop_time=$(date -u +%s)
elapsed=$(($stop_time - $start_time))
echo "FEniCS took $elapsed seconds to complete."
